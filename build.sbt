import scala.sys.process.Process

ThisBuild / scalaVersion := "2.13.10"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "petomat"
ThisBuild / organizationName := "petomat"

val zioVersion = "2.0.6"

val rsyncDeploy: InputKey[Unit] = inputKey[Unit]("Use rsync and ssh for deploying.")
val runRemote: InputKey[Unit] = inputKey[Unit]("Use run the app remotely")

//val ip = "raspbi32.zentrale.local" // 10.0.108.183
//val account = s"pi@$ip"
//val pwd = "pi"
//val home = "/home/pi/"
//val jdkHome = home
val ip = "10.0.108.39"
val account = s"application@$ip"
val pwd = "app"
val home = "/home/application/"
val jdkHome = "/data/"

val jdk11 = s"${jdkHome}zulu11.60.19-ca-jdk11.0.17-linux_aarch32hf"
val jdk17 = s"${jdkHome}zulu17.40.19-ca-jdk17.0.6-linux_aarch32hf"


val root = (project in file("."))
  .settings(
    name := "ZIOModbus",
    libraryDependencies ++= Seq(
      "dev.zio" %% "zio" % zioVersion,
      "dev.zio" %% "zio-streams" % zioVersion,
      "dev.zio" %% "zio-test" % zioVersion % Test,
      "dev.zio" %% "zio-test-sbt" % zioVersion % Test,
//      "dev.zio" %% "zio-test-magnolia" % zioVersion % Test,
      "dev.zio" %% "zio-process" % "0.7.1", // % Test,
      "com.fazecast" % "jSerialComm" % "2.9.3"
    ),
    resolvers += "PureJavaComm Mvn Repo" at "https://www.sparetimelabs.com/maven2/",
    libraryDependencies += "com.sparetimelabs" % "purejavacomm" % "0.0.22",
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"),
    reStart / mainClass := Some("Main"),
    assembly / mainClass := Some("Main"),
    rsyncDeploy := {
      val log = streams.value.log
      val src: File = assembly.value
      log.info(s"Copy assembly to remote $ip...")
      Process(Seq("sshpass", "-p", pwd, "rsync", "-P", src.toString, s"$account:$home/")) ! log
    },
    // split our application from provided dep libs via:
    // assemblyPackageScala / assembleArtifact := false,
    // assemblyPackageDependency / assembleArtifact := false,
    // sbt "assembly;assemblyPackageDependency"
    // java -cp "ZIOModbus-assembly-0.1.0-SNAPSHOT.jar:ZIOModbus-assembly-0.1.0-SNAPSHOT-deps.jar" Main
    runRemote := {
      val log = streams.value.log
      val src: File = assembly.value
      log.info(s"Stopping old application ...")
      val stopCmdResult = {
        Process(Seq("sshpass", "-p", pwd, "ssh", account, "killall -9 java")) ! log
      }
      if (stopCmdResult != 0) {
        log.info(s"Couldn't stop old application...")
      }
      log.info(s"Start application ${src.name} ...")
      Process(Seq("sshpass", "-p", pwd, "ssh", account, s"$jdk17/bin/java", "-jar", s"$home/${src.name}")) ! log
    }
  )
