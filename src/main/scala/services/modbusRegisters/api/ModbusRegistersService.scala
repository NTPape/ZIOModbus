package services.modbusRegisters.api

import zio.Chunk
import zio.ZIO
import services.modbusRequestResponse.api.NumberOfRegisters
import services.modbusRequestResponse.api.RegisterAddress
import services.modbusRequestResponse.api.SlaveId


// Idee: Schreiben/Lesen auf "grossem" Register(=2-Bytes)-Array.
// Achtung: Das array layout im modbus slave erlaubt nicht beliebiges lesen, sondern hat ein paar Regeln:
// z.B. darf man nicht nur einen Teil eines definierten Blocks (firmware variable) lesen:
// Wenn man vorne anfängt aber nicht den ganzen Block liest, gibt's ein IllegalDataValue.
// Wenn man mittendrin anfängt und bis Blockende liest, gibt's ein IllegalDataAddress.
trait ModbusRegistersService {
  // FIXME: error type, z.b. weniger register geschrieben als gewünscht.
  def readRegisters(
    slaveId: SlaveId,
    registerOffset: RegisterAddress,
    numberOfRegisters: NumberOfRegisters
  ): ZIO[Any, String, Chunk[Register]]
  // FIXME: wo sicherstellen, dass nur maximal (256-1-1-2)/2=126 register bei modbus möglich sind.
  //       https://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf (Page 5)
  def writeRegisters(
    slaveId: SlaveId,
    registerOffset: RegisterAddress,
    registers: Chunk[Register]
  ): ZIO[Any, String, Unit]
}


object ModbusRegistersService {
  // Accessors:
  def readRegisters(
    slaveId: SlaveId,
    registerOffset: RegisterAddress,
    numberOfRegisters: NumberOfRegisters
  ): ZIO[ModbusRegistersService, String, Chunk[Register]] = {
    ZIO.serviceWithZIO[ModbusRegistersService](_.readRegisters(slaveId, registerOffset, numberOfRegisters))
  }
  def writeRegisters(
    slaveId: SlaveId,
    registerOffset: RegisterAddress,
    registers: Chunk[Register]
  ): ZIO[ModbusRegistersService, String, Unit] = {
    ZIO.serviceWithZIO[ModbusRegistersService](_.writeRegisters(slaveId, registerOffset, registers))
  }
}
