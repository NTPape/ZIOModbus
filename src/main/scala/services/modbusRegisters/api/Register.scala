package services.modbusRegisters.api

import zio.Chunk
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants


case class Register private(bytes: Chunk[Byte]) extends AnyVal

object Register {
  def fromBytes(chunk: Chunk[Byte]): Register = {
    require(
      chunk.length == ModbusMessageConstants.ByteLengths.register,
      s"Unexpected register bytes, expected ${ModbusMessageConstants.ByteLengths.register} bytes," +
        s" but got ${chunk.length}"
    )
    Register(chunk)
  }
}