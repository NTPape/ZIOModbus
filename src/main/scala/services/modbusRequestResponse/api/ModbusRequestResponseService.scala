package services.modbusRequestResponse.api

import services.modbusRequestResponse.api.protocol.ModbusRequest
import services.modbusRequestResponse.api.protocol.ModbusResponse
import zio.Chunk
import zio.Tag
import zio.ZIO

trait ModbusRequestResponseService[Req <: ModbusRequest[Resp], Resp <: ModbusResponse] {
  // Envelope: Who = slaveId, What = request
  def request(slaveId: SlaveId, request: Req): ZIO[Any, ModbusRequestResponseService.Error, request.Response]
}

object ModbusRequestResponseService {
  // same for all protocols?:
  sealed trait Error extends Product with Serializable
  object Error {

    /** The slave id of the response does not match the slave of the request. */
    case class SlaveMismatch(expected: SlaveId, actual: Chunk[Byte]) extends Error

    /** The expected response could not be parsed to a valid response. */
    case object ResponseParsing extends Error

    /** The expected exception could not be parsed to a valid exception. */
    case object ExceptionParsing extends Error

    /** The returned function / error code does not match the request. */
    case class FunctionErrorCodeMismatch(
      code: Chunk[Byte],
      expectedFunctionCode: protocol.FunctionCode,
      expectedErrorCode: protocol.ErrorCode
    ) extends Error

    /** The request returned with a valid exception response. */
    case class Modbus(error: ModbusResponse.ExceptionResponse) extends Error

    /** The CRC checksum did not match. */
    case object CRC extends Error

    /** The max payload for the request was exceeded. */
    case object PayloadExceeded extends Error
  }
  // Accessors:
  def request[Req <: ModbusRequest[Resp]: Tag, Resp <: ModbusResponse: Tag](
    slaveId: SlaveId,
    request: Req
  ): ZIO[ModbusRequestResponseService[Req, Resp], ModbusRequestResponseService.Error, request.Response] = {
    ZIO.serviceWithZIO[ModbusRequestResponseService[Req, Resp]](_.request(slaveId, request))
  }
}
