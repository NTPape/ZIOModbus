package services.modbusRequestResponse.api

// FIXME yeah, number of versus count vs length (vs amount)
case class NumberOfRegisters private (short: Short) extends AnyVal

object NumberOfRegisters {
  def apply(short: Short): NumberOfRegisters = {
    require(short > 0, s"NumberOfRegisters must be greater than zero, but was $short")
    new NumberOfRegisters(short)
  }
}
