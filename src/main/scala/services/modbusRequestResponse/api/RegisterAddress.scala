package services.modbusRequestResponse.api

case class RegisterAddress private (short: Short) extends AnyVal

object RegisterAddress {
  def apply(short: Short): RegisterAddress = {
    require(short >= 0, s"Register address must be greater than or equal to zero, but was $short")
    new RegisterAddress(short)
  }
}
