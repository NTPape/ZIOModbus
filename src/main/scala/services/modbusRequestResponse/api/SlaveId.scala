package services.modbusRequestResponse.api

import zio.Chunk

case class SlaveId private (byte: Byte) extends AnyVal {

  def toBytes: Chunk[Byte] = Chunk(byte)
}

object SlaveId {
  def apply(byte: Byte): SlaveId = {
    // FIXME: some modbus limit and 0 is for broadcast
    require(byte > 0, s"Slave Id must be greater than zero, but was $byte")
    new SlaveId(byte)
  }

  object FromBytes {
    def unapply(bytes: Chunk[Byte]): Option[SlaveId] = bytes match {
      case Chunk(byte) if byte > 0 => Some(SlaveId(byte))
      case _ => None
    }
  }
}
