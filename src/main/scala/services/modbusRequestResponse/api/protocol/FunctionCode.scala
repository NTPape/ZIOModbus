package services.modbusRequestResponse.api.protocol

import zio.Chunk

case class FunctionCode private (byte: Byte) extends AnyVal {

  /** Returns the corresponding error code. */
  def errorCode: ErrorCode = {
    // In an exception response the used function code is exactly 80 hexadecimal higher than the value would be for a
    // normal response. This method returns the corresponding error code for this function code.
    // Bytes are signed in Java, so this will overflow to the negative range, but it will work fine.
    ErrorCode((byte + 0x80).toByte)
  }

  def toBytes: Chunk[Byte] = Chunk(byte)
}

object FunctionCode {

  // the latter is technically not possible because all numeric types are signed in Scala.
  def isValid(byte: Byte): Boolean = byte > 0 && byte <= 0x80

  def apply(byte: Byte): FunctionCode = {
    require(isValid(byte), s"Function code must be between zero (excl.) and 0x80 (incl.), but was $byte")
    new FunctionCode(byte)
  }

  object FromBytes {
    def unapply(bytes: Chunk[Byte]): Option[FunctionCode] = bytes match {
      case Chunk(byte) if isValid(byte) => Some(FunctionCode(byte))
      case _ => None
    }
  }
}

case class ErrorCode private (byte: Byte) extends AnyVal

object ErrorCode {

  // the latter is technically not possible because all numeric types are signed in Scala.
  def isValid(byte: Byte): Boolean = byte <= 0 || byte > 0x80

  def apply(byte: Byte): ErrorCode = {
    require(isValid(byte), s"Error code must not be between 0 (excl.) and 0x80 (incl.), but was $byte")
    new ErrorCode(byte)
  }

  object FromBytes {
    def unapply(bytes: Chunk[Byte]): Option[ErrorCode] = bytes match {
      case Chunk(byte) if isValid(byte) => Some(ErrorCode(byte))
      case _ => None
    }
  }
}
