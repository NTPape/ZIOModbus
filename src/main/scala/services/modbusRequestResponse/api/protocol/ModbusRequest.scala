package services.modbusRequestResponse.api.protocol

import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.ByteLengths
import zio.Chunk
import zio.Trace

// R = Upper bound for all responses of the protocol that is the parent trait extending ModbusResponse
// FIXME: variance needed?
// Request / Response bytes do not contain the slave Id nor the CRC, but the function code.
trait ModbusRequest[R <: ModbusResponse] {
  type Response <: R

  /** The function code of this request. */
  val functionCode: FunctionCode

  /** The error code of this request. */
  final val errorCode: ErrorCode = functionCode.errorCode

  /** The request body data, without function code or other envelope data (slave id and CRC.) */
  def requestData: Chunk[Byte]

  // FIXME how to efficiently assert(requestData.length <= 256 - 1 - 1 - 2) see https://modbus.org/docs/Modbus_Application_Protocol_V1_1b.pdf (Page 5)
  /** The whole payload of this request, without envelope data (slave id and CRC.) */
  final def payload: Chunk[Byte] = functionCode.toBytes ++ requestData

  /** Parses the response body. Bytes does not contain slave id, function code or CRC. */
  def createResponseFromBytes(bytes: Chunk[Byte])(implicit trace: Trace): Option[Response]

  /** The length of the expected successful response body. */
  def responseBodyLength: Int

  /** The whole response length without envelope of slave id and CRC. */
  final def responseLength: Int = ByteLengths.functionOrErrorCode + responseBodyLength

  /** The set of possible modbus exception codes for this request. */
  def possibleExceptionCodes: Set[ModbusResponse.ExceptionCode]
}
