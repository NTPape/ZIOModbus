package services.modbusRequestResponse.impl

import services.modbusRequestResponse.api.ModbusRequestResponseService
import services.modbusRequestResponse.api.SlaveId
import services.modbusRequestResponse.api.protocol.ErrorCode
import services.modbusRequestResponse.api.protocol.FunctionCode
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.ByteLengths
import services.modbusRequestResponse.api.protocol.ModbusRequest
import services.modbusRequestResponse.api.protocol.ModbusResponse
import services.serialPort.api.SerialPortService
import zio._

final class DefaultModbusRequestResponseService[Req <: ModbusRequest[Resp], Resp <: ModbusResponse](
  serialPortService: SerialPortService
) extends ModbusRequestResponseService[Req, Resp] {
  def request(slaveId: SlaveId, request: Req): ZIO[Any, ModbusRequestResponseService.Error, request.Response] = {
    // https://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf
    def RequestEnvelope(slaveId: SlaveId, request: Req): Chunk[Byte] = {
      val rawBytes: Chunk[Byte] = slaveId.toBytes ++ request.payload
      val crc16: CRC16 = CRC16(rawBytes)
      rawBytes ++ crc16.toBytes
    }
    def ResponseEnvelopeLength(request: Req): Int = ByteLengths.slaveId + request.responseLength + ByteLengths.crcLength

    val requestEnvelope = RequestEnvelope(slaveId, request)
    val sendRequest: ZIO[Any, ModbusRequestResponseService.Error, Unit] = {
      for {
        _ <- ZIO
          .fail(ModbusRequestResponseService.Error.PayloadExceeded)
          .when(requestEnvelope.length > ByteLengths.maxSerialBuffer)
        _ <- Console.printLine(s"Write ${requestEnvelope.length} ...").orDie
        _ <-
          serialPortService.writeBytes(requestEnvelope).orElse(
            ZIO.dieMessage("Could not write bytes")
          ) // FIXME: error handling
      } yield ()
    }
    val readResponseBytes: ZIO[Any, Nothing, Chunk[Byte]] = {
      for {
        responseBytesWithCRC <- serialPortService.readBytesAtMost(ResponseEnvelopeLength(request))
          //                                .debugConsoleBy(r => "responseBytesWithCRC length = " + r.length.toString)
          .orElse(ZIO.dieMessage("\nCould not read response bytes")) // FIXME: error handling
        // FIXME: fail if no bytes read or better less than for at least an error response
        _ <- ZIO.dieMessage("\nNo response bytes").when(responseBytesWithCRC.length <= 0)
      } yield responseBytesWithCRC
    }
    def checkCRC(responseBytesWithCRC: Chunk[Byte]) = {
      ZIO.fromOption(CRC16.check(responseBytesWithCRC)).orElseFail(ModbusRequestResponseService.Error.CRC)
    }
    def deserializeResponse(responseBytesWithoutCRC: Chunk[Byte]) = {
      responseBytesWithoutCRC.splitAt(ByteLengths.slaveId) match {
        case (SlaveId.FromBytes(`slaveId`), responseBytesWithoutSlaveIdCRC) =>
          responseBytesWithoutSlaveIdCRC.splitAt(ByteLengths.functionOrErrorCode) match {
            case (FunctionCode.FromBytes(request.functionCode), tail) =>
              ZIO.fromOption(request.createResponseFromBytes(tail))
                .orElseFail(ModbusRequestResponseService.Error.ResponseParsing)
            case (ErrorCode.FromBytes(request.errorCode), tail) =>
              tail match {
                case ModbusResponse.ExceptionCode.FromBytes(exceptionCode)
                  if request.possibleExceptionCodes.contains(exceptionCode) =>
                  ZIO.fail(ModbusRequestResponseService.Error.Modbus(
                    ModbusResponse.ExceptionResponse(request.functionCode, exceptionCode)
                  ))
                case _ =>
                  ZIO.fail(ModbusRequestResponseService.Error.ExceptionParsing)
              }
            case (other, _) =>
              ZIO.fail(
                ModbusRequestResponseService.Error.FunctionErrorCodeMismatch(
                  other,
                  request.functionCode,
                  request.errorCode
                )
              )
          }
        case (returnedSlaveIdBytes, _) =>
          ZIO.fail(ModbusRequestResponseService.Error.SlaveMismatch(slaveId, returnedSlaveIdBytes))
      }
    }
    for {
      _ <- sendRequest
      responseBytesWithCRC <- readResponseBytes
      responseBytesWithoutCRC <- checkCRC(responseBytesWithCRC)
      _ <- ZIO
        .dieMessage(s"\nPayload is unexpectedly small: ${responseBytesWithoutCRC}")
        .when(responseBytesWithoutCRC.length < ByteLengths.slaveId + ByteLengths.functionOrErrorCode)
      response <- deserializeResponse(responseBytesWithoutCRC)
    } yield response
  }
}

object DefaultModbusRequestResponseService {
  def live[
    Req <: ModbusRequest[Resp]: Tag, Resp <: ModbusResponse: Tag
  ]: ZLayer[SerialPortService, Nothing, ModbusRequestResponseService[Req, Resp]] =
    ZLayer.fromFunction { sps: SerialPortService => new DefaultModbusRequestResponseService[Req, Resp](sps) }
}
