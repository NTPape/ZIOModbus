package services.modbusRequestResponse.impl.protocol

import services.modbusRegisters.api.Register
import services.modbusRequestResponse.api.NumberOfRegisters
import services.modbusRequestResponse.api.RegisterAddress
import services.modbusRequestResponse.api.protocol
import services.modbusRequestResponse.api.protocol.FunctionCode
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants.ByteLengths
import services.modbusRequestResponse.api.protocol.ModbusProtocol
import services.modbusRequestResponse.api.protocol.ModbusRequest
import services.modbusRequestResponse.api.protocol.ModbusResponse
import services.modbusRequestResponse.api.protocol.ModbusResponse.ExceptionCode
import zio.Chunk
import zio.Trace

case object RWModbusProtocol extends ModbusProtocol {
  sealed trait Request extends ModbusRequest[Response]
  object Request {
    private[this] def shortToBytes(value: Short): Chunk[Byte] = {
      // FIXME are these "& 0xFF" needed?
      Chunk(((value >>> 8) & 0xff).toByte, (value & 0xff).toByte)
    }
    case class ReadHoldingRegisters(registerOffset: RegisterAddress, numberOfRegisters: NumberOfRegisters)
      extends Request {
      type Response = Response.ReadHoldingRegisters
      val functionCode: FunctionCode = FunctionCode(0x03)
      def requestData: Chunk[Byte] = {
        // Modbus specification reserves two bytes for register lengths
        shortToBytes(registerOffset.short) ++ shortToBytes(numberOfRegisters.short)
      }
      // bytes are without slave Id, function code and without crc
      def createResponseFromBytes(bytes: Chunk[Byte])(implicit trace: Trace): Option[Response] = {
        if (bytes.length != responseBodyLength) {
          None
        } else {
          val (byteCountBytes, registerBytes) = bytes.splitAt(ByteLengths.byteCount)
          val registerCount: Int = {
            assert(byteCountBytes.length == 1)
            (byteCountBytes.head & 0xff) / ByteLengths.register
          }
          // Check consistency of byte counts field:
          if (registerCount * ByteLengths.register != registerBytes.length) {
            None
          } else {
            val registers: Chunk[Register] = {
              Chunk.fromIterator(registerBytes.grouped(ByteLengths.register).map(Register.fromBytes))
            }
            Some(Response.ReadHoldingRegisters(registers))
          }
        }
      }
      // bytes are without envelope, i.e. without slave Id and without crc
      val responseBodyLength: Int = {
        ByteLengths.byteCount + numberOfRegisters.short * ByteLengths.register
      }

      val possibleExceptionCodes: Set[ExceptionCode] = Set(
        ExceptionCode.IllegalFunction,
        ExceptionCode.IllegalDataAddress,
        ExceptionCode.IllegalDataValue,
        ExceptionCode.SlaveDeviceFailure
      )
    }
  }
  sealed trait Response extends ModbusResponse
  object Response {
    case class ReadHoldingRegisters(registers: Chunk[Register]) extends Response
  }
}
