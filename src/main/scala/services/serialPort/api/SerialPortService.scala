package services.serialPort.api

import zio.Chunk
import zio.ZIO

trait SerialPortService {
  // FIXME: better name, more scaladoc, if stream closed than error case
  def readBytesAtMost(count: Int): ZIO[Any, String, Chunk[Byte]]
  def writeBytes(bytes: Chunk[Byte]): ZIO[Any, String, Unit]
}

object SerialPortService {

  def readBytesAtMost(count: Int): ZIO[SerialPortService, String, Chunk[Byte]] =
    ZIO.serviceWithZIO[SerialPortService](_.readBytesAtMost(count))

  def writeBytes(bytes: Chunk[Byte]): ZIO[SerialPortService, String, Unit] =
    ZIO.serviceWithZIO[SerialPortService](_.writeBytes(bytes))
}
