package services.serialPort.impl

import com.fazecast.jSerialComm.SerialPort
import com.fazecast.jSerialComm.SerialPortDataListener
import com.fazecast.jSerialComm.SerialPortEvent
import com.fazecast.jSerialComm.SerialPortInvalidPortException
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants
import services.serialPort.api.SerialPortService
// FIXME: port in ref statt semaphore. Auch für reconnect gut
// FIXME: error type!
import utils._
import zio.Chunk
import zio.Clock
import zio.Console
import zio.Schedule
import zio.Scope
import zio.Semaphore
import zio.ZIO
import zio.ZLayer
import zio.durationInt

// TODO: error type!
final class JSerialCommSerialPortService(
  val port: SerialPort,
  val semaphore: Semaphore
) extends SerialPortService {

  // FIXME: This is dependent on the semaphore permit count.
  private[this] val arr = new Array[Byte](ModbusMessageConstants.ByteLengths.maxSerialBuffer)

  private[this] def enrichError[R, A](zio: ZIO[R, String, A]): ZIO[R, String, A] =
    zio.mapError(s => s"Serial port '${port.getPortDescription}' : $s")

  def readBytesAtMost(amount: Int): ZIO[Any, String, Chunk[Byte]] = {
    println(s"${scala.Console.BLUE}PETER: JSerialCommSerialPortService.read: $amount ${scala.Console.RESET}")
    amount match {
      case n if n < 0 =>
        ZIO.fail("could not read a negative amount of bytes")
      case n if n > ModbusMessageConstants.ByteLengths.maxSerialBuffer =>
        ZIO.fail("could not read a too large amount of bytes")
      case 0 =>
        ZIO.succeed(Chunk.empty[Byte])
      case _ =>
        // FIXME: reuse array for performance
        for {
          _ <- Console.printLine(s"Reading $amount bytes...").orDie
          read <- ZIO.succeedBlocking(port.readBytes(arr, amount))
          bs <- read match {
            case -1 => ZIO.fail("There was an error reading from the port")
            case n => ZIO.succeed(Chunk.fromArray(arr.take(n)))
          }
        } yield bs
    }
  }

  def writeBytes(bytes: Chunk[Byte]): ZIO[Any, String, Unit] =
    semaphore.withPermit {
      enrichError {
        val len = bytes.length
        for {
          // FIXME: blocking buffering and so on, see https://github.com/Fazecast/jSerialComm/wiki/Blocking-and-Semiblocking-Reading-Usage-Example
          // FIXME: ZIO.attemptBlockingInterrupt or alike
          written <- ZIO.succeedBlocking(port.writeBytes(bytes.toArray, len))
          _ <- written match {
            case `len` => ZIO.unit
            case -1 => ZIO.fail("There was an error writing to the port") // FIXME: end of stream
            case n if n > len =>
              // FIXME: according to docu not possible
              ZIO.dieMessage("More bytes were unexpectedly written than available")
            case n => ZIO.fail(s"Only $n of $len bytes were written") // FIXME: write remaining...
          }
          // FIXME:  ZIO.attemptBlockingInterrupt or alike
          _ <- ZIO.succeedBlocking(port.flushIOBuffers()).filterOrFail(identity)("Could not flush written bytes")
        } yield ()
      }
    }
}

object JSerialCommSerialPortService {
  def scoped(port: String): ZIO[Scope, RuntimeException, SerialPortService] = {
    val acquire: ZIO[Any, RuntimeException, JSerialCommSerialPortService] = {
      // FIXME: ZIO.attemptBlockingInterrupt or alike
      ZIO.blocking {
        for {
          p <- ZIO.attempt(SerialPort.getCommPort(port)).refineToOrDie[SerialPortInvalidPortException]
          _ <- ZIO
            .succeed(
              p.openPort( /*safetySleepTime =*/ 0, /*deviceSendQueueSize =*/ 0, /*deviceReceiveQueueSize =*/ 0)
            ).filterOrDie(identity)(
              new IllegalStateException("Could not successfully open the port with a valid configuration.")
            )
          _ <- ZIO
            .succeed(
              p.setComPortParameters( /*baudRate = */ 115200, /*dataBits =*/ 8, /*stopBits =*/ 2, /*parity =*/ 0)
            ).filterOrDie(identity)(
              new IllegalStateException("The given configuration is disallowed on the system.")
            )
          _ <- ZIO
            .succeed(
              p.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0)
            ).filterOrDie(identity)(
              new IllegalStateException("The given configuration is disallowed on the system.")
            )
          /*
            _ <- ZIO.succeed(p.setRs485ModeParameters(true, true, false, false, 1, 0))
            _ <- ZIO.succeed(p.setRTS())
            _ <- ZIO.succeed(p.addDataListener(new SerialPortDataListener {
              override def getListeningEvents: Int = {
                SerialPort.LISTENING_EVENT_DATA_AVAILABLE | SerialPort.LISTENING_EVENT_DATA_RECEIVED | SerialPort.LISTENING_EVENT_DATA_WRITTEN | SerialPort.LISTENING_EVENT_PORT_DISCONNECTED
              }

              override def serialEvent(event: SerialPortEvent): Unit = {
                println(event)
                println(event.getReceivedData.mkString(" "))
              }
            }))
           */
          _ <- Clock.sleep(1.second)
          // FIXME: flush stale data?!! ugly:
          bytesAvailable <- ZIO.attempt(p.bytesAvailable()).debug("bytes to flush").orDie
          a = new Array[Byte](bytesAvailable)
          _ <- ZIO.attempt(p.readBytes(a, bytesAvailable, 0)).debug("Read stale data bytes").orDie
          s <- Semaphore.make(permits = 1)
        } yield {
          new JSerialCommSerialPortService(p, s)
        }
      }
    }
    val release: JSerialCommSerialPortService => ZIO[Any, Nothing, Unit] = { service =>
      ZIO
        .succeedBlocking(service.port.closePort())
        .flatMap {
          case true => ZIO.unit
          case false => ZIO.fail(())
        }
        .retry(Schedule.spaced(100.millis) && Schedule.recurs(3))
        // FIXME: where is the log message feeded to?
        .orElseSucceed(ZIO.logError("Could not close port"))
    }
    def portInfo(s: JSerialCommSerialPortService) = s"${s.port} (${s.port.getSystemPortName})"
    ZIO.acquireRelease(
      ZIO.debugBeforeAfter(s"Opening port $port...")(acquire)(s => s"Opened ${portInfo(s)}")
    ) { s =>
      ZIO.debugBeforeAfter(s"Closing port ${s.port}...")(release(s))(_ => s"Closed ${portInfo(s)}")
    }
  }
  // FIXME: port via config?
  def live(port: String): ZLayer[Any, RuntimeException, SerialPortService] = ZLayer.scoped(scoped(port))
}
