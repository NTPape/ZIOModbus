package services.serialPort.impl

import purejavacomm.CommPortIdentifier
import purejavacomm.NoSuchPortException
import purejavacomm.SerialPort
import purejavacomm.UnsupportedCommOperationException
import services.modbusRequestResponse.api.protocol.ModbusMessageConstants
import services.serialPort.api.SerialPortService
import utils._
import zio.Chunk
import zio.Scope
import zio.Semaphore
import zio.Task
import zio.ZIO
import zio.ZLayer

// FIXME: error type!
final class PureJavaCommSerialPortService(
  val serialPort: SerialPort,
  // FIXME: port in zio.Ref mit ref.modify statt semaphore. Auch für reconnect gut, d.h. service bleibt der gleiche nur der serivce kann den port wiederverbinden.
  val semaphore: Semaphore
) extends SerialPortService {
  // FIXME whr dann auch in Ref zusammen mit serial port
  private[this] val arr = new Array[Byte](ModbusMessageConstants.ByteLengths.maxSerialBuffer)
  def readBytesAtMost(count: Int): ZIO[Any, String, Chunk[Byte]] = {
    ZIO.attempt {
      serialPort.enableReceiveThreshold(count)
      val read: Int = serialPort.getInputStream.read(arr, 0, count)
      Chunk.fromArray(arr).take(read)
    }.orDie.debugTimedToConsoleBy { b => s"Read ${b.length} bytes" }
  }
  def writeBytes(bytes: Chunk[Byte]): ZIO[Any, String, Unit] = {
    ZIO.attempt(serialPort.getOutputStream.write(bytes.toArray)).orDie.debugTimedToConsoleBy { _ =>
      s"Wrote ${bytes.length} bytes"
    }
  }
}

object PureJavaCommSerialPortService {
  def scoped(port: String): ZIO[Scope, Exception, SerialPortService] = {
    val acquire: ZIO[Any, Exception, PureJavaCommSerialPortService] = {
      // FIXME config:
      val readTimeoutInMs = 50
      // FIXME: ZIO.attemptBlockingInterrupt or alike
      ZIO.blocking {
        for {
          serialPort <- ZIO.attempt(CommPortIdentifier.getPortIdentifier(port).open("appname", 1000))
            .refineToOrDie[NoSuchPortException]
            .filterTypeOrElse[SerialPort](ZIO.dieMessage("commPort isn't a serial port"))
          _ <- ZIO.attempt(serialPort.setSerialPortParams(115200, 8, 2, 0))
            .refineToOrDie[UnsupportedCommOperationException]
//          _ <- ZIO.attempt(Rs485ModeSetting.setRs485Mode(port)).orDie
          _ <- ZIO.attempt(serialPort.enableReceiveTimeout(readTimeoutInMs)).orDie
//          // FIXME: flush stale data?!! ugly:
//          a  = new Array[Byte](512)
//          _ <- ZIO.attempt(serialPort.getInputStream.read(a, 512, 0)).debugTimedToConsoleBy { r => s"Read $r stale data bytes" }.orDie
          s <- Semaphore.make(permits = 1)
        } yield {
          new PureJavaCommSerialPortService(serialPort, s)
        }
      }
    }
    val release: PureJavaCommSerialPortService => ZIO[Any, Nothing, Unit] = { service =>
      ZIO.succeedBlocking(service.serialPort.close())
    }
    def portInfo(s: PureJavaCommSerialPortService) = s.serialPort.getName
    ZIO.acquireRelease(
      ZIO.debugBeforeAfter(s"Opening port $port...")(acquire)(s => s"Opened ${portInfo(s)}")
    ) { s =>
      ZIO.debugBeforeAfter(s"Closing port ${portInfo(s)}...")(release(s))(_ => s"Closed ${portInfo(s)}")
    }
  }
  // FIXME: port via config?
  def live(port: String): ZLayer[Any, Exception, SerialPortService] = ZLayer.scoped(scoped(port))
}
