package services.serialPort.impl

import com.sun.jna.Platform
import jtermios.JTermios._
import purejavacomm.NoSuchPortException


/**
 * Object to implement the static function to configure a tty port as rs485 port.
 * This is only supported on Linux systems.
 * From serial.h :
 * {{{
 *   Serial interface for controlling RS485 settings on chips with suitable
 *   support. Set with TIOCSRS485 and get with TIOCGRS485 if supported by your
 *   platform. The set function returns the new state, with any unsupported bits
 *   reverted appropriately.
 *
 *   struct serial_rs485 {
 *     __u32 flags; /* RS485 feature flags */
 *     #define SER_RS485_ENABLED (1 << 0) /* If enabled */
 *     #define SER_RS485_RTS_ON_SEND (1 << 1) /* Logical level for RTS pin when sending */
 *     #define SER_RS485_RTS_AFTER_SEND (1 << 2) /* Logical level for RTS pin after sent*/
 *     #define SER_RS485_RX_DURING_TX (1 << 4)
 *     __u32 delay_rts_before_send; /* Delay before send (milliseconds) */
 *     __u32 delay_rts_after_send; /* Delay after send (milliseconds) */
 *     __u32 padding[5]; /* Memory is cheap, new structs are a royal PITA .. */
 *   };
 * }}}
 */
object Rs485ModeSetting {

  private[this] class Rs485Exception(message: String) extends Exception(message)

  /* ioctl to read back current configuration */
  private[this] val TIOCGRS485 = 0x542E
  /* ioctl to write serial settings */
  private[this] val TIOCSRS485 = 0x542F
  /*
   * enable RS485 mode: depending on the hardware
   * additional hw features of the uart will be used (e.g. echo suppression)
   */
  private[this] val SER_RS485_ENABLED = 1 << 0
  /* use the RS pin to switch between transmitting/receiving */
  private[this] val SER_RS485_RTS_ON_SEND = 1 << 1
  /* full duplex flag */
  private[this] val SER_RS485_RX_DURING_TX = 1 << 4

  /* struct serial_rs485 implemented as array of integers */
  private[this] val serial_rs485 = new Array[Int](8)


  /**
   * Sets rs485 specific configuration flags
   *
   * This is only supported on Linux/Android platforms and ignored on all other platforms.
   *
   * @param portIdString the Device Name (e.g /dev/ttyUSB1)
   */
  def setRs485Mode(portIdString : String): Unit = {

    if (Platform.isAndroid || Platform.isLinux) {

      val fd = open(portIdString, O_RDWR | O_NOCTTY | O_NONBLOCK)
      /* if we can not open the device abort */
      if (fd < 0) {
        throw new NoSuchPortException
      }
      try {

        if (ioctl(fd, TIOCGRS485, serial_rs485) < 0) {
          throw new Rs485Exception("RS485 TIOCGRS485 not supported")
        }

        /*
         * configuration : half duplex with RTS gpio for
         * transmitter enable/disable
         */
        serial_rs485(0) &= ~SER_RS485_RX_DURING_TX
        serial_rs485(0) |= (SER_RS485_ENABLED | SER_RS485_RTS_ON_SEND)

        if (ioctl(fd, TIOCSRS485, serial_rs485) < 0) {
          throw new Rs485Exception("RS485 TIOCSRS485 not supported")
        }

      } catch {
        case e: Rs485Exception =>
          Console.err.println(s"unsupported ioctl: ${e.getMessage}")
      } finally {
        jtermios.JTermios.close(fd)
      }
    }
  }
}
