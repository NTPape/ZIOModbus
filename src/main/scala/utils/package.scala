import zio.Cause
import zio.Chunk
import zio.Clock
import zio.Console
import zio.Duration
import zio.IO
import zio.Tag
import zio.Trace
import zio.UIO
import zio.ZIO

import java.io.IOException
import java.time.temporal.ChronoUnit
import java.util.concurrent.TimeUnit

import scala.reflect.ClassTag

package object utils {

  /**
   * Just wraps another value, so essentially a no-op, but it is helpful for, e.g. for comprehensions that
   * need to be processed further. So instead of (scalafmt formatted):
   * {{{
   * (
   *   for {
   *     _ <- ZIO.yieldNow
   *   } yield ()
   *   ).timed
   * }}}
   * or
   * {{{
   * (for {
   *   _ <- ZIO.yieldNow
   * } yield ()
   *   ).timed
   * }}}
   * we have
   * {{{
   * code {
   *   for {
   *     _ <- ZIO.yieldNow
   *   } yield ()
   * }.timed
   * }}}
   */
  @inline def code[T](block: T): T = block

  // FIXME: wieso geht das mit einem assignment in der zeile danach aber nicht direkt inline?
  implicit final class ZIOOps[R, E, A](private val zio: ZIO[R, E, A]) extends AnyVal {
    // equivalent to filterOrDie(p)(new NoSuchElementException())?
    def withFilter(p: A => Boolean)(implicit trace: Trace): ZIO[R, E, A] =
      zio.flatMap {
        case a if p(a) => ZIO.succeed(a)
        case _ => ZIO.die(new NoSuchElementException())
      }

    def debugToConsoleBy(f: A => String, g: Cause[E] => String = _.toString)(implicit trace: Trace): ZIO[R, E, A] =
      zio
        .tap(value => Console.printLine(f(value)).orDie)
        .tapErrorCause(error => Console.printLineError(s"<FAIL> ${g(error)}").orDie)

    def debugToConsole(prefix: => String)(implicit trace: Trace): ZIO[R, E, A] = {
      debugToConsoleBy(a => s"$prefix $a")
    }

    def debugTimedToConsoleBy(f: A => String)(implicit trace: Trace): ZIO[R, E, A] = {
      zio
        .timed
        .debugToConsoleBy { case (d, a) => s"${f(a)} took ${d.shortString}" }
        .map { case (d, a) => a }
    }

    def debugTimedToConsole(prefix: => String)(implicit trace: Trace): ZIO[R, E, A] = {
      debugTimedToConsoleBy(a => s"$prefix $a")
    }

    // "orElse" is more general than "orDie" or "orFail"
    def filterTypeOrElse[A1 <: A](z: ZIO[R, E, A1])(implicit ct: ClassTag[A1], trace: Trace): ZIO[R, E, A1] =
      zio.flatMap {
        case ct(a1) => ZIO.succeed(a1)
        case _ => z
      }

    def withConsoleZIO[B <: Console](console: => ZIO[R, E, B])(implicit tag: Tag[B], trace: Trace): ZIO[R, E, A] =
      console.flatMap(zio.withConsole(_))

    def withDebugConsole(implicit trace: Trace): ZIO[R, E, A] = {
      withConsoleZIO(TimedConsole.create)
    }
  }

  implicit class ZIOCompanionOps(private val companion: ZIO.type) extends AnyVal {
    // FIXME better name
    def debugBeforeAfter[R, E, A](before: String)(zio: ZIO[R, E, A])(after: A => String)(implicit trace: Trace): ZIO[R,
      E, A] =
      for {
        _ <- Console.printLine(before).orDie
        a <- zio
        _ <- Console.printLine(after(a)).orDie
      } yield a
  }

  implicit class DurationOps(private val duration: Duration) extends AnyVal {
    def logString: String = {
      val millis = TimeUnit.NANOSECONDS.toMillis(duration.toNanos)
      val h = (millis % 86400000) / 3600000
      val m = (millis % 3600000) / 60000
      val s = (millis % 60000) / 1000
      val ms = millis % 1000
      f"[$h%02d:$m%02d:$s%02d.$ms%03d]"
    }
    def shortString: String = {
      val totalMillis = TimeUnit.NANOSECONDS.toMillis(duration.toNanos)
      val h = (totalMillis % 86400000) / 3600000
      val m = (totalMillis % 3600000) / 60000
      val s = (totalMillis % 60000) / 1000
      val ms = totalMillis % 1000
      val micros = TimeUnit.NANOSECONDS.toMicros(duration.toNanos) % 1000
      val sb = new StringBuilder
      var delimiterNeeded = false
      def addDelimiterIfNeeded(): Unit = {
        if (delimiterNeeded) {
          sb.append(" ")
          delimiterNeeded = false
        }
      }
      if (h != 0) {
        // addDelimiterIfNeeded()
        sb.append(h)
        sb.append("h")
        delimiterNeeded = true
      }
      if (m != 0) {
        addDelimiterIfNeeded()
        sb.append(m)
        sb.append("m")
        delimiterNeeded = true
      }
      if (s != 0) {
        addDelimiterIfNeeded()
        sb.append(s)
        sb.append("s")
        delimiterNeeded = true
      }
      if (ms != 0 || micros != 0) {
        addDelimiterIfNeeded()
        sb.append(f"$ms.$micros%03dms")
        // delimiterNeeded = true
      }
      sb.result()
    }
  }

  case class TimedConsole private (console: Console, consoleStart: Duration) extends Console {
    private[this] def prefixTime(a: Any): UIO[String] = {
      import TimedConsole.millisUnit
      Clock.currentTime(millisUnit).map { ms =>
        val d = Duration(ms, millisUnit) minus consoleStart
        s"${d.logString} : $a"
      }
    }

    def print(line: => Any)(implicit trace: Trace): IO[IOException, Unit] =
      prefixTime(line).flatMap(console.print(_))

    def printError(line: => Any)(implicit trace: Trace): IO[IOException, Unit] =
      prefixTime(line).flatMap(console.printError(_))

    def printLine(line: => Any)(implicit trace: Trace): IO[IOException, Unit] =
      prefixTime(line).flatMap(console.printLine(_))

    def printLineError(line: => Any)(implicit trace: Trace): IO[IOException, Unit] =
      prefixTime(line).flatMap(console.printLineError(_))

    def readLine(implicit trace: Trace): IO[IOException, String] =
      console.readLine
  }

  object TimedConsole {
    private val millisUnit = ChronoUnit.MILLIS
    def create(implicit trace: Trace): ZIO[Any, Nothing, TimedConsole] =
      for {
        ms <- Clock.currentTime(millisUnit)
        c <- ZIO.console
      } yield TimedConsole(c, Duration(ms, millisUnit))
  }
}
